package steps;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.leaftap.lead.PreandPost;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead extends PreandPost{
	//ChromeDriver driver;

//	@Given("startApplication")
//	public void startapplication() {
//		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
//		 driver = new ChromeDriver();
//		
//		driver.get("http://leaftaps.com/opentaps/");
//		driver.manage().window().maximize(); 
//		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//	}
//
//	@Given("Login")
//	public void login() {
//		driver.findElementById("username").sendKeys("DemoSalesManager");
//		driver.findElementById("password").sendKeys("crmsfa");
//		driver.findElementByClassName("decorativeSubmit").click();
		
//	}

	@Given("Navigate to CreateLead Module")
	public void navigateToCreateLeadModule() {
		driver.findElementByLinkText("CRM/SFA").click();
		
		driver.findElementByLinkText("Create Lead").click();   
	}

	@Given("enter mandatory details {string} {string} {string}")
	public void enterMandatoryDetails(String cm, String fn, String ln) {
		driver.findElementById("createLeadForm_companyName").sendKeys(cm);
		driver.findElementById("createLeadForm_firstName").sendKeys(fn);
		driver.findElementById("createLeadForm_lastName").sendKeys(ln);
		
		WebElement element = driver.findElementById("createLeadForm_dataSourceId");
		Select source = new Select(element);
		source.selectByVisibleText("Direct Mail");
		
		WebElement market = driver.findElementById("createLeadForm_marketingCampaignId");
		Select camp=new Select(market);
		List<WebElement> options = camp.getOptions();
//		int size = options.size();
		camp.selectByIndex((options.size())-1);
	 
	}



	@When("click on create button")
	public void clickOnCreateButton() {
		driver.findElementByName("submitButton").click();
		//driver.findElementByLinkText("Create Lead").click();   
	}

	@Then("verify Lead creation is success")
	public void verifyLeadCreationIsSuccess() {
	    System.out.println("Created Lead successfully");
	}
	
	

@Given("Navigate to FindLead")
public void navigateToFindLead() {
	driver.findElementByLinkText("CRM/SFA").click();

	driver.findElementByXPath("//a[text()='Leads']").click();
	driver.findElementByXPath("//a[text()='Find Leads']").click();
	
}

@Given("SearchCriteria and click on the respective record")
public void searchcriteriaAndClickOnTheRespectiveRecord() throws InterruptedException {
	driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Divya");
	driver.findElementByXPath("(//td[@class='x-btn-center'])[7]").click();
	Thread.sleep(3000);
	driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
	String title = driver.getTitle();
	if (title.equals("View Lead | opentaps CRM"))
	{
		System.out.println("Title Verified - Passed" + title);
	}
	else
	{
		System.out.println("Title Verified - Failed" +title);
	}
	
}

@Given("click on edit button")
public void clickOnEditButton() {
	driver.findElementByXPath("//a[text()='Edit']").click();
	
}

@Given("do the changes")
public void doTheChanges() {
	driver.findElementByXPath("(//input[@name='companyName'])[2]").clear();
	driver.findElementByXPath("(//input[@name='companyName'])[2]").sendKeys("WIPRO");
}

@When("Click on update")
public void clickOnUpdate() {
	driver.findElementByXPath("//input[@value='Update']").click();
}

@Then("verify changes are displays successfully")
public void verifyChangesAreDisplaysSuccessfully() {
	WebElement compName = driver.findElementByXPath("//span[@id='viewLead_companyName_sp']");
	
	if(compName.getText().startsWith("WIPRO"))
	{
		System.out.println("Company name changed -Passed" + compName.getText());
		}
		else
	{
			System.out.println("Company name not changed - Failed" + compName.getText());
	}

}
}

	
	

