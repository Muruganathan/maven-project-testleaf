Feature: CreateLead in LeafTap 
Background: 
	Given startApplication 
Given Login 
Scenario Outline: LeadCreation 
	Given Navigate to CreateLead Module 
	And enter mandatory details "<cm>" "<fn>" "<ln>" 
	When click on create button 
	Then verify Lead creation is success 
	Examples: 
		|cm|fn|ln|
		|wipro|Divya|M|
		|Google|Arun|N|
		|Oracle|Janhav|A|
Scenario: EditLead
Given Navigate to FindLead
Given SearchCriteria and click on the respective record
Given click on edit button 
And do the changes
When Click on update
Then verify changes are displays successfully


