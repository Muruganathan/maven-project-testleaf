package hooks;

import org.leaftap.lead.PreandPost;
import org.openqa.selenium.OutputType;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.AfterStep;
import cucumber.api.java.Before;

public class Hooks extends PreandPost{

	@Before
	public void before()
	{
		
		login();
	}
	@After
public void after(Scenario sc)
{
		if(sc.isFailed())
		{
			byte[] screenshotAs = driver.getScreenshotAs(OutputType.BYTES);
			sc.embed(screenshotAs, "image/png");
		}
	closeBrowser();
}
	@AfterStep
	public void afterstep(Scenario sc)
	{
		byte[] screenshotAs = driver.getScreenshotAs(OutputType.BYTES);
		sc.embed(screenshotAs, "image/png");
	}
}
