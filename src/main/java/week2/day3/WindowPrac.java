package week2.day3;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class WindowPrac {
public static ChromeDriver driver;
	public static void main(String[] args) {
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	driver = new ChromeDriver();
	driver.get("http://leafground.com/pages/Window.html");
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	
	driver.findElementByXPath("//button[text()='Open Multiple Windows']").click();
	String mwin=driver.getWindowHandle();
	Set<String> win=driver.getWindowHandles();
	String main = driver.switchTo().window(mwin).getTitle();
	
//	List<String> lwin=new ArrayList<>(win);
//	System.out.println(lwin.size());
	
/*	for(int i=1;i<lwin.size();i++)
	{
	String tit1 = driver.switchTo().window(lwin.get(i)).getTitle();
	System.out.println("Title of window : "+tit1);
	}*/
	
	
	for(String lwin:win)
	{
		String title = driver.switchTo().window(lwin).getTitle();
		if (!title.equals(main))
		{
			System.out.println("Title of window : "+title);
		}
		
		
		
	}
	

	driver.quit();
	}

}
