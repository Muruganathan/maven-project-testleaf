package week2.day1;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class ImplicitSnap {

public static ChromeDriver driver;
 static int i=0;
	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.google.co.in/");
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.findElementByXPath("//input[@type='text']").sendKeys("Selenium"+Keys.ENTER);
		
ImplicitSnap.takesnap();
ImplicitSnap.takesnap();
	}

	public static void takesnap() throws IOException
	{
		File snap = driver.getScreenshotAs(OutputType.FILE);
		File dest = new File("./snaps/img"+i+".png");
		i++;
		FileUtils.copyFile(snap, dest);
		
	}
}
