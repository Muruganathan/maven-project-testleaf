package week2.day1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.support.ui.Select;

public class SetConcept {

	public static void main(String[] args) {
		Set<String> se=new HashSet<>();
		se.add("Muruganathan");
		se.add("Santhi");
		se.add("Suguna");
		se.add("Divya");
		se.add("Santhi");
//		System.out.println(se);
		List<String> li=new ArrayList<>();
		li.addAll(se);
		System.out.println("Printing all values");
		System.out.println(li);
		System.out.println("Value at Position 3 - " +li.get(2));
		
	}

}
