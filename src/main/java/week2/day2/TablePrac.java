package week2.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TablePrac {
public static ChromeDriver driver;
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		
		driver.get("http://leafground.com/pages/table.html");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		WebElement tab = driver.findElementByXPath("//table[@cellspacing='0']");
		
		List<WebElement> rows = tab.findElements(By.tagName("tr"));
		System.out.println("No.Of.Rows : " +rows.size());
		
		int cols = rows.get(1).findElements(By.tagName("td")).size();
		System.out.println("No.Of.Cols: "+ cols);
		
		for(int i =1;i<rows.size();i++)
		{
			List<WebElement> colval = rows.get(i).findElements(By.tagName("td"));
		String reqtext = colval.get(0).getText();
				if(reqtext.contains("Elements"))
				{
					System.out.println("Progress Value= "+colval.get(1).getText());
					colval.get(2).click();
					break;
				}
		}
			
	}

}
