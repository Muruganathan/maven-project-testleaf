package week2.day2;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class ActionsPract {
public static ChromeDriver driver;
	public static void main(String[] args) throws IOException {
		int i=0;
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver  = new ChromeDriver();
		driver.get("http://leafground.com/pages/drag.html");
		WebElement drag = driver.findElementById("draggable");
		Actions builder = new Actions(driver);
		builder.dragAndDropBy(drag, 100, 100).perform();
		File snap = driver.getScreenshotAs(OutputType.FILE);
		File dest = new File("./snaps/leaf/actions"+i+".png");
	    FileUtils.copyFile(snap, dest);
		i++;
		driver.navigate().to("http://leafground.com/pages/selectable.html");
		WebElement li1 = driver.findElementByXPath("//li[text()='Item 1']");
		WebElement li2 = driver.findElementByXPath("//li[text()='Item 2']");
		WebElement li3 = driver.findElementByXPath("//li[text()='Item 3']");
		builder.clickAndHold(li1).clickAndHold(li2).clickAndHold(li3).perform();
		File snap1 = driver.getScreenshotAs(OutputType.FILE);
		File dest1 = new File("./snaps/leaf/actions"+i+".png");
	    FileUtils.copyFile(snap1, dest1);
	    i++;

	}

}
