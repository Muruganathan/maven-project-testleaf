package week2.day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class IrctcCheck {

	public static ChromeDriver driver;
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://erail.in/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS"+Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("TPJ"+Keys.TAB);
		driver.findElementById("chkSelectDateOnly").click();
		
		WebElement tab = driver.findElementByXPath("//table[@class='DataTable TrainList TrainListHeader']");
		List<WebElement> rows = tab.findElements(By.tagName("tr"));
		List<Integer> tn=new ArrayList<>();
		for(int i = 0;i<rows.size();i++)
		{
			List<WebElement> cols = rows.get(i).findElements(By.tagName("td"));
			//List<Integer> tn=new ArrayList<>();
			String colval = cols.get(0).getText();
				tn.add(Integer.parseInt(colval));
//			System.out.println(tn);
		}
		Collections.sort(tn);
		//System.out.println(tn);
		for(int i=0;i<tn.size();i++)
		{
		System.out.println("Sorting the Train Numbers : "+tn.get(i));
		}
	driver.findElementByXPath("//a[@title='Click here to sort on Train Number']").click();
	WebElement tab1 = driver.findElementByXPath("//table[@class='DataTable TrainList TrainListHeader']");
	List<WebElement> rows1 = tab1.findElements(By.tagName("tr"));
	System.out.println(rows1.size());

	for(int i=0;i<rows1.size();i++)
	{
		List<WebElement> cols1 = rows1.get(i).findElements(By.tagName("td"));
		//System.out.println(cols1);
		String colval1 = cols1.get(0).getText();	
		if (colval1.equals(tn.get(i)))
		{
		System.out.println("Numbers not sorted properly");
		break;
		}
	}
	System.out.println("Numbers sorted properly");
	Set se=new TreeSet<>();
	se.addAll(tn);
	if (se.size()==rows.size())
	System.out.println("There are no duplicates");
	driver.close();
	}
	
}
	/***	
	List<WebElement> cols1 = rows1.get(0).findElements(By.tagName("td"));
	System.out.println(cols1.get(0).getText());
	String colval1 = cols1.get(0).getText();	
	System.out.println(colval1);
	}
	*/

	

