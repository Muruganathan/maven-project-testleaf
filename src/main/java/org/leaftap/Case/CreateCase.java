package org.leaftap.Case;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class CreateCase {
public static ChromeDriver driver;
@Test
	public void main() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		 driver = new ChromeDriver();
		
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		
		driver.findElementByXPath("//a[text()='Cases']").click();
		driver.findElementByXPath("//a[text()='Create Case']").click();
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
		
		Set<String> win= driver.getWindowHandles();
		List<String> lwin=new ArrayList<>(win);
		driver.switchTo().window(lwin.get(1));
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.switchTo().window(lwin.get(0));
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
		
		Set<String> win1= driver.getWindowHandles();
		List<String> lwin1=new ArrayList<>(win1);
		driver.switchTo().window(lwin1.get(1));
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.switchTo().window(lwin1.get(0));
		
		WebElement sel = driver.findElementById("createCaseForm_priority");
		Select selpri=new Select(sel);
		selpri.selectByVisibleText("Critical (1)");
		
		WebElement sel1 = driver.findElementById("createCaseForm_custRequestTypeId");
		Select seltyp=new Select(sel1);
		seltyp.selectByVisibleText("Request For Information");
		
		WebElement sel2 = driver.findElementById("createCaseForm_custRequestCategoryId");
		Select selrea=new Select(sel2);
		selrea.selectByVisibleText("Instructions not clear");
		
		driver.findElementById("createCaseForm_custRequestName").sendKeys("Test");
		driver.findElementByName("submitButton").click();
		System.out.println(driver.getTitle());
		
		

	}

}
