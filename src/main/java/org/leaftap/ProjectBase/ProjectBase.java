package org.leaftap.ProjectBase;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class ProjectBase {
	public static ChromeDriver driver;

	public static void startApp(String url)
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver= new ChromeDriver();
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	public static void closeApp()
	{
		driver.close();
	}
	public static WebElement eleloc(String locat,String path)
	{
		if(locat.equals("className"))
		{
			WebElement ele =  driver.findElement(By.className(path));
			 return ele;
		}
		else if(locat.equals("cssSelector"))
		{
			WebElement ele =  driver.findElement(By.cssSelector(path));
			 return ele;
		}
		else if(locat.equals("id"))
		{
			return  driver.findElement(By.id(path));
			 
		}
		else if(locat.equals("linkText"))
		{
			WebElement ele =  driver.findElement(By.linkText(path));
			 return ele;
		}
		else if(locat.equals("name"))
		{
			WebElement ele =  driver.findElement(By.name(path));
			 return ele;
		}
		else if(locat.equals("partialLinkText"))
		{
			WebElement ele =driver.findElement(By.partialLinkText(path));
			 return ele;
		}
		else if(locat.equals("tagName"))
		{
			WebElement ele = driver.findElement(By.tagName(path));
			 return ele;
		}
		else 
		{
			 WebElement ele = driver.findElement(By.xpath(path));
			 return ele;
		}
		 
	}
	public static void appendtext(WebElement ele, String data)
	{
		ele.sendKeys(data);
	}
	public static void cleartext(WebElement ele, String data)
	{
		ele.clear();
	}
	
	public void tab(WebElement target) {
		Actions builder=new Actions(driver);
		builder.sendKeys(Keys.CONTROL).click(target).perform();

	}
	public static void KeyMouse(String act,WebElement ele,String Keys)
	{
		Actions builder=new Actions(driver);
		if(act.equals("click"))
		{
			builder.click(ele);
		}
		else if(act.equals("clickAndHold"))
		{
			builder.click(ele);
		}
		else if(act.equals("contextClick"))
				{
			builder.contextClick(ele).perform();
			
				}
		
	else if(act.equals("doubleClick"))
	{
		builder.doubleClick(ele);
	}
	else if(act.equals("release"))
	{
		builder.release();
	}
	else if(act.equals("tick"))
	{
		builder.tick();
		
	}
	else if(act.equals("sendKeys"))
	{
		//CharSequence keys = "T";
		
		builder.sendKeys(Keys);
		
	}
	else if(act.equals("keyDown"))
	{
		//CharSequence keys = "T";
	//	builder.keyDown();
		
	}
		
	else
	{
		
		System.out.println("Mouse Actions has to be define yet in KeyMouse method");
	}
	}
	
	public static void multiwincon(int index)
	{
		Set<String> win=driver.getWindowHandles();
		List<String>lwin=new ArrayList<>(win);
		driver.switchTo().window(lwin.get(index));
	}
	public static void wincon()
	{
		String main=driver.getWindowHandle();
		driver.switchTo().window(main);
	}
	
}
	
		
	
	
	
