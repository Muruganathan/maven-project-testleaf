package org.leaftap.contact;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class CrmContact {
@Test
	public void main() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		
		driver.findElementByLinkText("CRM/SFA").click();
		
		driver.findElementByLinkText("Create Contact").click();
		
		driver.findElementById("firstNameField").sendKeys("Arun");
		driver.findElementById("lastNameField").sendKeys("N");
		driver.findElementById("createContactForm_firstNameLocal").sendKeys("Kutty");
		driver.findElementById("createContactForm_lastNameLocal").sendKeys("N");
		driver.findElementById("createContactForm_personalTitle").sendKeys("Mr");
		driver.findElementById("createContactForm_generalProfTitle").sendKeys("Dear");
		driver.findElementById("createContactForm_departmentName").sendKeys("Security");
		driver.findElementById("createContactForm_preferredCurrencyUomId").sendKeys("INR - Indian Rupee");
		driver.findElementById("createContactForm_description").sendKeys("Security Analyst");
		driver.findElementById("createContactForm_importantNote").sendKeys("Hacker");
		driver.findElementById("createContactForm_primaryPhoneNumber").sendKeys("9789943671");
		driver.findElementById("createContactForm_primaryPhoneAreaCode").sendKeys("54");
		driver.findElementById("createContactForm_primaryPhoneExtension").sendKeys("044");
		driver.findElementById("createContactForm_primaryPhoneAskForName").sendKeys("Arun N");
		driver.findElementById("createContactForm_primaryEmail").sendKeys("arun.rad@gmail.com");
		driver.findElementById("generalToNameField").sendKeys("Bravo");
		driver.findElementById("createContactForm_generalAttnName").sendKeys("Arun Divya");
		driver.findElementById("createContactForm_generalAddress1").sendKeys("Adambakkam");
		driver.findElementById("createContactForm_generalAddress2").sendKeys("St Thomas");
		driver.findElementById("createContactForm_generalCity").sendKeys("Chennai");
		driver.findElementById("createContactForm_generalPostalCode").sendKeys("600054");
		driver.findElementById("createContactForm_generalPostalCodeExt").sendKeys("044");
		driver.findElementById("createContactForm_generalCountryGeoId").sendKeys("India");
		Thread.sleep(3000);
		driver.findElementById("createContactForm_generalStateProvinceGeoId").sendKeys("TAMILNADU");
		
		driver.findElementByName("submitButton").click();
	    System.out.println(driver.getTitle());
	}
}