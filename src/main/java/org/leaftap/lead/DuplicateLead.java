package org.leaftap.lead;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class DuplicateLead extends PreandPost {
@Test
	public void duplicate() throws InterruptedException {
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByXPath("//a[text()='Leads']").click();
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByXPath("//span[text()='Email']").click();
		driver.findElementByXPath("//input[@name='emailAddress']").sendKeys("kumar85962@mail.com");
		Thread.sleep(2000);
		driver.findElementByXPath("(//td[@class='x-btn-center'])[7]").click();
		Thread.sleep(2000);
		WebElement fn = driver.findElementByXPath("(//a[@class='linktext'])[6]");
		String fname = fn.getText();
		System.out.println("First name Captured "+fname);
		driver.findElementByXPath("(//a[@class='linktext'])[6]").click();
		driver.findElementByXPath("//a[text()='Duplicate Lead']").click();
		driver.findElementByXPath("//div[text()='Duplicate Lead']");
		String title = driver.getTitle();
		if (title.startsWith("Duplicate Lead"))
		{
			System.out.println("Title Verified - " +title);
		}
		else
		{
			System.out.println("Title Verification failed - " +title);
		}
		driver.findElementByXPath("//input[@value='Create Lead']").click();
		WebElement dn = driver.findElementByXPath("//span[@id='viewLead_firstName_sp']");
		String dfn = dn.getText();
		if (dfn.equals(fname))
		{
			System.out.println("Duplicated name is as same as Captured name - " + dfn);
		}
		else
		{
			System.out.println("Duplicated name is not as same as Captured name - " + dfn);
		}
//driver.close();
	}

}
