package org.leaftap.lead;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class EditLead extends PreandPost {
	//ChromeDriver driver;
@Test
	public  void edit() throws InterruptedException {
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByXPath("//a[text()='Leads']").click();
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Divya");
		driver.findElementByXPath("(//td[@class='x-btn-center'])[7]").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		String title = driver.getTitle();
		if (title.equals("View Lead | opentaps CRM"))
		{
			System.out.println("Title Verified - Passed" + title);
		}
		else
		{
			System.out.println("Title Verified - Failed" +title);
		}
		driver.findElementByXPath("//a[text()='Edit']").click();
		driver.findElementByXPath("(//input[@name='companyName'])[2]").clear();
		driver.findElementByXPath("(//input[@name='companyName'])[2]").sendKeys("WIPRO");
		driver.findElementByXPath("//input[@value='Update']").click();
		WebElement compName = driver.findElementByXPath("//span[@id='viewLead_companyName_sp']");
				
	if(compName.getText().startsWith("WIPRO"))
	{
		System.out.println("Company name changed -Passed" + compName.getText());
		}
		else
	{
			System.out.println("Company name not changed - Failed" + compName.getText());
	}
		
			

	}

}
