package org.leaftap.lead;

import java.util.List;

import org.leaftap.utils.TestData;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CreateCrmLead extends PreandPost{

	@BeforeClass
	public void getexcelname()
	{
	//	excelfilename="CreateLead";
	}
@Test(dataProvider="datasupply")
	public void create(String cn,String FN,String LN) {
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys(cn);
		driver.findElementById("createLeadForm_firstName").sendKeys(FN);
		driver.findElementById("createLeadForm_lastName").sendKeys(LN);
		
		WebElement element = driver.findElementById("createLeadForm_dataSourceId");
		Select source = new Select(element);
		source.selectByVisibleText("Direct Mail");
		
		WebElement market = driver.findElementById("createLeadForm_marketingCampaignId");
		Select camp=new Select(market);
		List<WebElement> options = camp.getOptions();
//		int size = options.size();
		camp.selectByIndex((options.size())-1);
		
		
driver.findElementByName("submitButton").click();
	}


			
}

