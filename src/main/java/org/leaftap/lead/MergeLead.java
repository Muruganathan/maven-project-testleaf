package org.leaftap.lead;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class MergeLead extends PreandPost {
//public static ChromeDriver driver;
@Test
	public  void Merge() throws InterruptedException {
		driver.findElementByLinkText("CRM/SFA").click();
		
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("//img[@alt='Lookup']").click();
		
		Set<String> win = driver.getWindowHandles();
		List<String> lwin=new ArrayList<>(win);
		driver.switchTo().window(lwin.get(1));
		//driver.findElementByName("id").sendKeys("10142");
		//driver.findElementByXPath("//button[text()='Find Leads']").click();
		//Thread.sleep(3000);
		/*WebElement ele = driver.findElementByXPath("(//a[@class='linktext'])[1]");
		ele.click();*/
		WebElement fromlead = driver.findElementByXPath("(//a[@class='linktext'])[1]");
		String fl = fromlead.getText();
		fromlead.click();
		driver.switchTo().window(lwin.get(0));
		Thread.sleep(3000);
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
		
		Set<String> swin = driver.getWindowHandles();
		List<String> lwin1=new ArrayList<>(swin);
		driver.switchTo().window(lwin1.get(1));
		//driver.findElementByName("id").sendKeys("10143");
		WebDriverWait wait = new WebDriverWait(driver,10);
		WebElement tolead = driver.findElementByXPath("(//a[@class='linktext'])[7]");
		wait.until(ExpectedConditions.elementToBeClickable(tolead));
		String tl = tolead.getText();
		tolead.click();
		//driver.findElementByXPath("//button[text()='Find Leads']").click();
		//Thread.sleep(3000);
		//driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.switchTo().window(lwin1.get(0));
		
		driver.findElementByXPath("//a[text()='Merge']").click();
		
		driver.switchTo().alert().accept();
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByName("id").sendKeys(fl);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		// WebElement check = driver.findElementByXPath("//a[text()='10142']");
		 
WebElement mes = driver.findElementByXPath("//div[text()='No records to display']");
		 System.out.println(mes.getText());
		 
	
	//	driver.close();


	}

}
