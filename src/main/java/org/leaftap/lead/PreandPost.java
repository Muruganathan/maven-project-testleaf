package org.leaftap.lead;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

//import org.leaftap.utils.CreateLeadTestData;
import org.leaftap.utils.TestData;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

public class PreandPost {
	//String excelfilename;
	public static ChromeDriver driver;
	//@Parameters({"username","password"})
@BeforeMethod
	public void login()
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		 driver = new ChromeDriver();
		
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		
	}
//@AfterTest
//@AfterMethod
	public void closeBrowser()
	{
		driver.close();
	}
}
