package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FlipKart {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.flipkart.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("//button[text()='✕']").click();
		Actions builder= new Actions(driver);
		WebDriverWait wait=new WebDriverWait(driver, 30);
		WebElement electronics = driver.findElementByXPath("//span[text()='Electronics']");
		WebElement mobiles = driver.findElementByXPath("//a[text()='Mobiles']");
		WebElement Mi = driver.findElementByXPath("(//a[text()='Mi'])[1]");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Electronics']")));
		builder.moveToElement(electronics).perform();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath	("//a[text()='Mobiles']")));
		builder.moveToElement(mobiles).perform();
//		wait.until(ExpectedConditions.presenceOfElementLocated((By) electronics));
		builder.moveToElement(Mi);
		builder.click().build().perform();
		String s="Mi Mobile";
wait.until(ExpectedConditions.titleContains(s));

//		Thread.sleep(30000);
	System.out.println(driver.getTitle());
/*WebElement title = driver.findElementByXPath("//h1[text()='Mi Mobiles']");
if (title.getText().equals("Mi Mobiles"))
{
System.out.println(title.getText() +" - Title Verfied");
}	*/	

	}

}
