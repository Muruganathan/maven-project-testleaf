package week4.day1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WriteExcel {

	public static void main(String[] args) throws IOException {
		//FileInputStream fis = new FileInputStream("./Data/DeleteLead.xlsx");
		XSSFWorkbook wrkbook = new XSSFWorkbook();
		XSSFSheet sheet = wrkbook.createSheet();
		XSSFRow row = sheet.createRow(1);
		XSSFCell cell = row.createCell(0);
		cell.setCellValue("Hi");
		FileOutputStream fout=new FileOutputStream(new File("./Data/DeleteLead.xlsx"));
		wrkbook.write(fout);
		wrkbook.close();
		

	}

}
