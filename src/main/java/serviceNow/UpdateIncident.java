package serviceNow;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class UpdateIncident {

	public static void main(String[] args) throws InterruptedException {
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	ChromeDriver driver=new ChromeDriver();
	driver.get("https://dev58255.service-now.com/");
	driver.switchTo().frame(0);
	driver.findElementByXPath("//input[@name='user_name']").sendKeys("admin");
	driver.findElementByXPath("//input[@name='user_password']").sendKeys("Arun@6310");
	driver.findElementByXPath("//button[@type='submit']").click();
	Thread.sleep(3000);
	driver.findElementByXPath("//input[@id='filter']").sendKeys(" incident");
	Thread.sleep(3000);
	driver.findElementByXPath("(//div[text()='Open - Unassigned'])[1]").click();
	driver.switchTo().frame(0);
	driver.findElementByXPath("//input[@class='form-control']").sendKeys("INC0006311"+Keys.ENTER);
	driver.findElementByXPath("(//a[@class='linked formlink'])[1]").click();
	WebElement con = driver.findElementByXPath("//select[@id='incident.contact_type']");
	Select cont=new Select(con);
	cont.selectByVisibleText("Self-service");
	driver.findElementByXPath("//button[@id='sysverb_update']").click();
	driver.switchTo().defaultContent();
	driver.findElementByXPath("(//div[text()='Open'])[1]").click();
	driver.switchTo().frame(0);
	driver.findElementByXPath("//input[@class='form-control']").sendKeys("INC0006311"+Keys.ENTER);
	
	WebElement incfir = driver.findElementByXPath("(//a[@class='linked formlink'])[1]");
	String incfirs = incfir.getText();
	if (incfirs.equals("INC0006311"))
			{
		System.out.println("Verified first incident number - passed" +incfirs);
			}
	else
	{
		System.out.println("Verified first incident number - Failed" +incfirs);
	}
	driver.findElementByXPath("(//a[@class='linked formlink'])[1]").click();
	WebElement verin = driver.findElementByXPath("//input[@id='incident.number']");
	String attr = verin.getAttribute("value");
	if (attr.equals(incfirs))
	{
		System.out.println("Verified result " +attr);
	}
	else
	{
		System.out.println("verified result - failed " +attr);
	}
	}
	
	
}
