package serviceNow;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateIncident {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.get("https://dev58255.service-now.com/");
		driver.switchTo().frame(0);
		driver.findElementByXPath("//input[@name='user_name']").sendKeys("admin");
		driver.findElementByXPath("//input[@name='user_password']").sendKeys("Arun@6310");
		driver.findElementByXPath("//button[@type='submit']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//input[@id='filter']").sendKeys(" incident");
		Thread.sleep(1000);
		driver.findElementByXPath("(//div[text()='Create New'])[1]").click();
		driver.switchTo().frame(0);
		driver.findElementByXPath("//input[@id='incident.number']").sendKeys("INC0006311");
		WebElement cdd = driver.findElementByXPath("//select[@id='incident.category']");
		Select cate= new Select(cdd);
		cate.selectByVisibleText("Network");
		Thread.sleep(2000);
		WebElement scdd = driver.findElementByXPath("//select[@id='incident.subcategory']");
		Select subcate=new Select(scdd);
		subcate.selectByVisibleText("Wireless");
		WebElement pr = driver.findElementByXPath("//select[@id='incident.impact']");
		Select pri = new Select(pr);
		pri.selectByIndex(1);
		WebElement upri = driver.findElementByXPath("//select[@id='incident.urgency']");
		Select uprior=new Select(upri);
		uprior.selectByValue("2");
		driver.findElementByXPath("//input[@id='sys_display.incident.caller_id']").sendKeys("Diana "+Keys.TAB);
		driver.findElementByXPath("//input[@id='incident.short_description']").sendKeys("Hii");
		Thread.sleep(2000);
		driver.findElementByXPath("//button[@id='sysverb_insert']").click();
		
		driver.findElementByXPath("(//input[@class='form-control'])[1]").sendKeys("INC0006311"+Keys.ENTER);
		Thread.sleep(2000);
		WebElement inci = driver.findElementByXPath("//a[contains(text(),'INC0006311')]");
		String text = inci.getText();
		if (text.equals("INC0006311"))
		{
			System.out.println("Incident created successfully-"+text );
		}
		else
		{
			System.out.println("Incident not created -"+text );
		}
		
		
	}

}
