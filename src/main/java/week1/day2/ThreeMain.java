/* Main method in Java is entry point for JVM to start the execution.Main method
 * in Java can be overloaded.In order to execute the overloaded main method, we
 * need to call it from actual main method.Example are below 
 */

package week1.day2;

public class ThreeMain {

	public static void main(String[] args) {
		System.out.println("Main Method called");
		ThreeMain.main("overloaded first Main");

	}
public static void main(String arg1)
{
	System.out.println("called "+arg1);
	ThreeMain.main("Called","overloaded 2nd Main");
}
public static void main(String arg1,String arg2)
{
	System.out.println(arg1 +arg2);
	ThreeMain.main(1,2);
}
public static void main(int arg2,int arg3)
{
	System.out.println(arg2+" "+arg3);
}

}
