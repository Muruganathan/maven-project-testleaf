/* Constructors are used to initialize the object�s state. Like methods, a constructor also contains collection of statements
 * (i.e. instructions) that are executed at time of Object creation.
 * 
    Constructor(s) of a class must has same name as the class name in which it resides.
    A constructor in Java can not be abstract, final, static and Synchronized.
    Access modifiers can be used in constructor declaration to control its access i.e which other class can call the constructor.

No-argument constructor: A constructor that has no parameter is known as default constructor. If we don�t define a constructor in a class, then compiler creates default constructor(with no arguments) for the class. And if we write a constructor with arguments or no-arguments then the compiler does not create a default constructor.
Default constructor provides the default values to the object like 0, null, etc. depending on the type.

Parameterized Constructor: A constructor that has parameters is known as parameterized constructor. If we want to initialize fields of the class with your own values, then use a parameterized constructor.
 */
package week1.day2;

public class Constructor {

		 Constructor()
		{
		System.out.println("Default Constructor Executed");	
		}
		Constructor(String s)
		{
			System.out.println("Single Parameterized constructor is executed" + s);
		}

		Constructor(int s)
		{
			System.out.println("Double Parameterized constructor is executed" + s);
		}
}
