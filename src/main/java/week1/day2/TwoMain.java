package week1.day2;

public class TwoMain {

	   public static void main(String[] args) { 
	        System.out.println("Hi Geek (from main)"); 
	        TwoMain.main("Geek"); 
	    } 
	  
	    // Overloaded main methods 
	    public static void main(String arg1) { 
	        System.out.println("Hi, " + arg1); 
	        TwoMain.main("Dear Geek","My Geek"); 
	    } 
	    public static void main(String arg1, String arg2) { 
	        System.out.println("Hi, " + arg1 + ", " + arg2); 
	    } 
	} 