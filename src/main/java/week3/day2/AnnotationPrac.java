package week3.day2;

import org.testng.annotations.Test;

public class AnnotationPrac {
	@Test(priority=1,dependsOnMethods="third")
public void first()
{
	System.out.println("first");
	
}
@Test(priority=-1)
public void second()
{
	System.out.println("second");
}
@Test(dependsOnMethods="second")
public void third()
{
	System.out.println("third");
}
}
