package week5.day1;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class PropertiesPra {
	public static ChromeDriver driver;

	public static void main(String[] args) throws FileNotFoundException, IOException {
		Properties prop=new Properties();
		prop.load(new FileInputStream("English.properties"));
		//String property = prop.getProperty("LoginPage.enterUserName.id");

		
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			 driver = new ChromeDriver();
			
			driver.get("http://leaftaps.com/opentaps/");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.findElementById(prop.getProperty("LoginPage.enterUserName.id")).sendKeys("DemoSalesManager");
			driver.findElementById(prop.getProperty("LoginPage.enterPassword.id")).sendKeys("crmsfa");
			driver.findElementByClassName(prop.getProperty("LoginPage.clickLogin.class")).click();
			
		
	}

}
